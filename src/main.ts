import { Component, createApp } from 'vue';
import App from './App.vue';
import 'element-plus/dist/index.css';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';

import CHINESE from 'element-plus/lib/locale/lang/zh-cn';
import { ElCol, ElRow } from 'element-plus';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const index = import.meta.env.VITE_APP_URL ?? '/api/index/index';

const app = createApp(App, { locale: CHINESE, index });

/**
 * 为动态使用Icon，全局注册
 */
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component('IEp' + key, component as Component);
}

app.component('ElRow', ElRow);
app.component('ElCol', ElCol);

app.mount('#app');
