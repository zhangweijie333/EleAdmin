/**
 * 模型文件
 */

import { Er, Face } from '../elementui';
import { WidgetName } from '../form/widgets';
import { CellName } from '../table/cells';

export type PascalName =
  | 'bool'
  | 'decimal'
  | 'int'
  | 'ip'
  | 'file'
  | 'select'
  | 'str'
  | 'text'
  | 'time'
  | 'date'
  | 'remote';

export interface Pascal extends Er {
  name: PascalName;

  widget?: WidgetName;
  cell?: CellName;
  face?: Face;
}

export const pascals: Record<PascalName, Pascal> = {
  bool: {
    icon: 'icon-switch',
    label: '布尔值',
    name: 'bool',
    widget: 'switch',
    cell: 'icon',
    face: 'warning'
  },
  int: {
    label: '整数数值',
    name: 'int',
    widget: 'number',
    icon: 'icon-slider',
    face: 'warning'
  },
  decimal: {
    label: '浮点数',
    name: 'decimal',
    icon: 'icon-number',
    face: 'warning'
  },
  time: {
    label: '时间',
    name: 'time',
    icon: 'icon-time',
    widget: 'time',
    face: ''
  },
  date: {
    label: '日期',
    name: 'date',
    icon: 'icon-date',
    widget: 'date',
    face: 'warning'
  },
  select: {
    label: '列表选择',
    name: 'select',
    icon: 'icon-select2',
    widget: 'select',
    face: 'danger'
  },
  remote: {
    label: '关联选择',
    name: 'remote',
    icon: 'icon-relation',
    widget: 'relation',
    face: 'danger'
  },
  ip: {
    label: 'IP地址',
    name: 'ip',
    icon: 'icon-link-url',
    face: 'info'
  },
  file: {
    label: '附件上传',
    name: 'file',
    icon: 'icon-attachment',
    widget: 'file',
    face: 'info'
  },
  str: {
    label: '字符文本',
    name: 'str',
    icon: 'icon-text-one',
    face: 'success'
  },
  text: {
    label: '富文本',
    name: 'text',
    widget: 'text',
    icon: 'icon-text-n',
    face: 'info'
  }
};
