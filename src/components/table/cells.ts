import { defineAsyncComponent } from 'vue';

const Cells = {
  span: defineAsyncComponent(() => import('./cell/SpanCell.vue')),
  pic: defineAsyncComponent(() => import('./cell/PicCell.vue')),
  url: defineAsyncComponent(() => import('./cell/UrlCell.vue')),
  tag: defineAsyncComponent(() => import('./cell/TagCell.vue')),
  icon: defineAsyncComponent(() => import('../icon/SwitchIcon.vue'))
};

export type CellName = keyof typeof Cells;

export const getCell = (name: CellName) => Cells[name] || Cells.span;
