/**
 * # 表单文件
 */
import { DisabledComponent, Er, Row } from '../elementui';
import { Toolbar } from '../button/button';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// import { RuleItem } from 'async-validator';
import { InnerWidget, WidgetName } from './widgets';
import { FormItemRule, FormRules } from 'element-plus';

// /**
//  * 单行文本 {{ type: 'text' | 'password', length: number }}
//  */
// export const INPUT_WIDGET = 'input-widget';
// /**
//  * 多行文本 {{ rows: number }}
//  */
// export const TEXT_WIDGET = 'text-widget';
// /**
//  * 关联选择
//  *   {{ model: select, multi: boolean, widget: select }}
//  */
// export const RELATION_WIDGET = 'relation-widget';
// /**
//  * 多项选择
//  *  {{ options: Option[] }}
//  */
// export const CHECKBOX_WIDGET = 'checkbox-widget';
// /**
//  * 单项选择 {{ button: boolean, options: Option[] }}
//  */
// export const RADIO_WIDGET = 'radio-widget';
// /**
//  * 下拉菜单
//  *   {{ options: Option[] }}
//  */
// export const SELECT_WIDGET = 'select-widget';
// /**
//  * 级联菜单
//  *   {{ options: Option[] }}
//  */
// export const CASCADER_WIDGET = 'cascader-widget';
//
// export const SWITCH_WIDGET = 'switch-widget';
// export const SLIDER_WIDGET = 'slider-widget';
// export const RATE_WIDGET = 'rate-widget';
// export const COLOR_WIDGET = 'color-widget';
//
// export const TIME_WIDGET = 'time-widget';
// export const DATE_WIDGET = 'date-widget';
//
// export const UPLOAD_WIDGET = 'upload-widget';
// export const AVATAR_WIDGET = 'avatar-widget';
//
// export const EDITOR_WIDGET = 'editor-widget';
// export const AUTO_WIDGET = 'auto-widget';
//
// export const PAIR_WIDGET = 'pair-widget';
//
// export const GROUP_WIDGET = 'group-widget';

/**
 * ## 表单
 */
export interface Form extends Er {
  /**
   * 表单域列表
   */
  fields: Array<Field>;
  /**
   * 校验信息
   */
  rules?: FormRules;

  /**
   * 表单宽度
   */
  style?: Record<string, unknown>;
  /**
   * 标签宽度
   */
  labelWidth?: string;
  /**
   * 标签位置
   */
  labelPosition?: string;

  /**
   * 绑定的数据
   */
  row?: Row;

  /**
   * 提交按钮 string: Url
   */
  submit?: string;
  /**
   * 重置按钮
   */
  reset?: string | false;
  /**
   * 操作按钮
   */
  toolbar?: Toolbar['items'];
  /**
   * 填表说明
   */
  description?: string;
}

// /**
//  * 验证元
//  */
// export interface RuleItemElement extends RuleItem {
//   trigger: 'blur' | 'change';
// }
// /**
//  * 表单/表单域验证配置对象
//  */
// export type RulesElement = Record<string, unknown>;

/**
 * select/radio/checkbox 选项列表
 */
export const OPTIONS = 'options';
/**
 * 选择组件选项数据配置
 */
export interface Option {
  label?: string;
  value?: number | string;
  disabled?: boolean;
}

/**
 * 域关联
 */
export interface Trigger {
  /**
   * [p: me.value]: { [px: string]: value }
   */
  options?: { [p: string]: { [p: string]: unknown } };
}

/**
 * ## 表单域
 */
export interface Field extends DisabledComponent, Er {
  /**
   * 组件名称
   */
  widget?: WidgetName | InnerWidget;
  /**
   * 校验信息
   */
  rules?: FormItemRule | FormItemRule[];
  /**
   * 默认值
   */
  default?: unknown;

  /**
   * 子表单域
   */
  fields?: Field[];

  /**
   * options 选项
   */
  [OPTIONS]?: Array<Option>;
  /**
   * options 选项多选
   */
  multi?: boolean;

  /**
   * 组
   */
  arrayed?: boolean;

  /**
   * 重复
   */
  repeat?: boolean;

  /**
   * 关联域数据自动填充
   */
  trigger?: Trigger;

  /**
   * row模式下的宽度
   */
  col?: number;

  required?: boolean;

  min?: number;
  max?: number;
  step?: number;
  showSteps?: boolean;

  activeColor?: string;
  inactiveColor?: string;

  // [p: string]: unknown;
}

export type WidgetOnCreate = 'select' | 'radio' | 'checkbox';

/**
 * ## 构件
 */
export interface Widget extends Er {
  name: WidgetName | 'group';
  /**
   * select/radio/checkbox options列表生成助手
   */
  onCreate?: WidgetOnCreate;

  [p: string]: unknown;
}

/**
 * 下拉选择类构件基础定义
 */
const select: Widget = {
  name: 'select',
  onCreate: 'select'
};

/**
 * ### 构件定义
 */
export const widgets: Record<WidgetName | 'group', Widget> = {
  cascader: {
    label: '级联菜单',
    name: 'cascader',
    icon: 'icon-cascader'
  },
  checkbox: {
    label: '多项选择',
    icon: 'icon-checkbox',
    name: 'checkbox',
    onCreate: 'checkbox'
  },
  select: {
    ...select,
    icon: 'icon-select',
    label: '下拉菜单'
  },
  radio: {
    label: '单项选择',
    icon: 'icon-radio',
    name: 'radio',
    onCreate: 'radio'
  },
  relation: {
    label: '关联',
    icon: 'icon-relation',
    name: 'relation'
  },
  color: {
    label: '色彩选择',
    name: 'color'
  },
  pair: {
    label: '选项组',
    name: 'pair'
  },
  file: {
    label: '文件上传',
    icon: 'icon-download',
    name: 'file'
  },
  date: {
    label: '日期选择',
    icon: 'icon-date',
    name: 'date'
  },
  number: {
    icon: 'icon-slider',
    label: '滑块',
    name: 'number'
  },
  time: {
    icon: 'icon-time',
    label: '时间选择',
    name: 'time'
  },
  switch: {
    label: '开关',
    icon: 'icon-switch',
    name: 'switch'
  },
  rate: {
    icon: 'icon-rate',
    label: '五星评价',
    name: 'rate'
  },
  input: {
    icon: 'icon-input',
    label: '单行文本',
    name: 'input'
  },
  text: {
    icon: 'icon-textarea',
    label: '多行文本',
    type: 'textarea',
    block: true,
    autosize: {
      minRows: 2,
      maxRows: 6
    },
    name: 'text'
  },
  group: {
    icon: 'el-group',
    label: '子表单',
    fields: [],
    name: 'group'
  }
};

/**
 * 获取组件默认值
 * @param {Field} field
 */
export function getDefaultValue(field: Field) {
  if (field.default !== undefined) return field.default;
  if (field.fields) return {};
  return undefined;
}
