import {
  coreCreator,
  FieldEvents,
  FormEvents,
  functional,
  getLayoutComponent,
  groupCreator,
  labelCreator,
  mainCreator,
  Wrapper,
  WrapperParam,
  YNode
} from './wrapper';
import { rowArrayWrapper, rowLabelWrapper } from './RowLayout';
import { Field } from '../form';
import { computed, h } from 'vue';
import ElColSeDe from './ElColSeDe.vue';
import { MakerSlotParam } from '../../maker/structure';
import DraggableWrapper from '../../maker/DraggableWrapper.vue';
import { ElCol, ElRow } from 'element-plus';
import { Row } from '../../elementUI';

export const draggableCoreWrapper: Wrapper<FieldEvents, Field, YNode[]> = (param) => {
  const { events, attrs, field, index } = param;
  return [
    h(
      ElColSeDe,
      {
        index,
        span: field.col || 24,
        selected: attrs?.selected,
        onSelect: events?.select,
        onRemove: events?.remove
      },
      functional(coreCreator(param))
    )
  ];
};

export const draggableGroupWrapper: Wrapper<FieldEvents> = (param) => {
  const labelNode = labelCreator(param);

  const children = groupCreator(param);

  const { events, attrs } = param;

  return [
    h(
      ElColSeDe,
      {
        span: 24,
        selected: attrs?.selected,
        onSelect: events?.select,
        onRemove: events?.remove
      },
      functional([
        h(ElRow, null, functional(h(ElCol, { span: 24 }, functional(labelNode)))),
        ...(children || [])
      ])
    )
  ];
};

const draggableMainWrapper: Wrapper<FormEvents, Field[]> = (param) => {
  const { field: fields, value, events, context, attrs } = param;

  return [
    h(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      DraggableWrapper,
      {
        collection: attrs?.collection,
        // component: 'ElRow',
        tag: 'div',
        class: { 'el-row': true },
        elements: fields,
        'onUpdate:elements': events?.elements,
        modelValue: value
      },
      {
        default: ({ selected, select, remove }: MakerSlotParam) =>
          mainCreator({
            field: fields,
            value,
            attrs: {
              ...attrs,
              selected
            },
            events: {
              ...(events || {}),
              select,
              remove
            },
            context
          }) || []
      }
    )
  ];
};

export default getLayoutComponent((props, { emit, slots, attrs }) => {
  const param = computed<WrapperParam<FormEvents, Field[]>>(() => {
    return {
      field: props.fields || [],
      value: props.modelValue as Row,
      events: {
        modelValue: (v) => emit('update:modelValue', v),
        elements: (v) => emit('update:elements', v)
      },
      attrs,
      context: {
        slots,
        wrappers: {
          labelWrapper: rowLabelWrapper,
          groupWrapper: draggableGroupWrapper,
          coreWrapper: draggableCoreWrapper,
          arrayWrapper: rowArrayWrapper,
          mainWrapper: draggableMainWrapper
        }
      }
    } as WrapperParam<FormEvents, Field[]>;
  });
  return draggableMainWrapper(param.value);
});
