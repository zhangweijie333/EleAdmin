import { Field, Option, WidgetOnCreate } from '../form';
import { ElCheckbox, ElOption, ElRadio, ElRadioButton } from 'element-plus';
import type { Component } from 'vue';
import { WidgetName } from '../widgets';

/**
 * 选择组件选项
 */
export interface OptionConfig extends Option {
  tag: string | Component;
  text?: string | number;
}

/**
 * select|radio|checkbox options列表生成助手
 */
export type OnCreateOptions = (option: Option, field?: Field) => OptionConfig;

export const FieldOptionCreators: Record<WidgetOnCreate, OnCreateOptions> = {
  select: (option: Option) => ({
    tag: ElOption,
    label: option?.label + '',
    value: option?.value,
    disabled: option?.disabled
  }),
  checkbox: (option: Option) => ({
    tag: ElCheckbox,
    ...(option || {}),
    label: option?.value + '',
    text: option?.label
  }),
  radio: (option: Option, field?: Field & { button?: boolean }) => ({
    tag: field?.button ? ElRadioButton : ElRadio,
    // ...option,
    label: option?.value + '',
    text: option?.label
  })
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export const getOptions = (name: WidgetName) => FieldOptionCreators[name] ?? false;
